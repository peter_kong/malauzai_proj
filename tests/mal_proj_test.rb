require 'test/unit'
require_relative '../lib/malauzai_proj'

class MyTest < Test::Unit::TestCase
  include MalProj

  LOCATIONS = [
      [30.298149,-97.723412],
      [30.285400,-97.739985],
      [30.259573,-97.767630]
  ]

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    # Do nothing
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  def test_customer_a
    locs = get_locations('Customer A', LOCATIONS[0].first, LOCATIONS[0].last)
    assert_equal 200, locs.size
    assert_kind_of(Array, locs)
    assert locs.first.xpath('//geometry')
    result_1 = locs[0].xpath('//vicinity').text
    result_21 = locs[20].xpath('//vicinity').text
    result_41 = locs[40].xpath('//vicinity').text
    assert_not_equal result_1, result_21
    assert_not_equal result_1, result_41
    assert_not_equal result_41, result_21
  end

  def test_customer_b
    locs = get_locations('Customer B', LOCATIONS[0].first, LOCATIONS[0].last)
    assert_equal 20, locs.size
    assert_kind_of(Array, locs)
    assert_kind_of(Hash, locs.first)
    assert locs.first['geometry']
  end

  def test_customer_c
    locs = get_locations('Customer C', LOCATIONS[0].first, LOCATIONS[0].last)
    assert_equal 5, locs.size
    assert_kind_of(Array, locs)
    assert_kind_of(Hash, locs.first)
    assert locs.first['geometry']
  end
end