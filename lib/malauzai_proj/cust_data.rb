
CUSTOMERS = {
    'Customer A' => {
        type: 'atm',
        language: 'English',
        output: 'xml',
        num_locations: 200},

    'Customer B' => {
        type: 'bank',
        language: 'Spanish',
        output: 'json',
        num_locations: 20},

    'Customer C' => {
        type: 'all',
        language: 'French',
        output: 'json',
        num_locations: 5}
}