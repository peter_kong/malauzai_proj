require 'open-uri'
require 'json'
require 'nokogiri'
require_relative 'malauzai_proj/malauzai_proj_consts'
require_relative 'malauzai_proj/cust_data'

module MalProj
  # @param cust_name [String] name of customer e.g. 'Customer A'
  # @param latitude [Float]
  # @param longitude [Float]
  # @return [Array] Array of locations in either XML or json format
  def get_locations(cust_name, latitude, longitude)
    customer = CUSTOMERS[cust_name]
    output = customer[:output]
    location_str = get_location_str(latitude, longitude)
    options = {
        language: customer[:language]
    }
    types = get_loc_types customer
    results = []
    page_token = nil
    until results.size >= customer[:num_locations] do
      results += types.collect do |t|
        options[:type] = t
        response = page_token ?
            get_more_results(output, page_token) :
            get_first_api_response(output, location_str, options)
        ary, page_token = send("#{output}_to_array", response)
        ary
      end.flatten
    end

    results[0...customer[:num_locations]]
  end

  private

  # @param output [String] 'json' or 'xml'
  # @param location_str [String] lat/longitude as comma-separated string
  # @param options [Hash] Google API options
  # @return [File] json or XML response as file
  def get_first_api_response(output, location_str, options)
    open get_search_url(output, location_str, options)
  end

  # @param output [String] 'json' or 'xml'
  # @param page_token [String] for requesting more than 20 results from Google API; note, method sleeps 2 seconds to prevent invalid request
  # @return [File] json or XML response as File
  def get_more_results(output, page_token)
    sleep 2 # prevents invalid requests
    open "#{GOOGLE_URL}#{output}?pagetoken=#{page_token}&key=#{API_KEY}"
  end

  # @param customer [Hash] hash must contain key :type with value 'all', 'atm', or 'bank'
  # @return [Array] of String
  def get_loc_types(customer)
    case customer[:type]
      when 'all'          then  return %w(atm bank)
      when 'atm', 'bank'  then  return [customer[:type]]
      else
        throw Exception, "location type must be 'atm', 'bank' or 'all'"
    end
  end

  # @param output [String] 'json' or 'xml'
  # @param key [String] Google API key
  # @param location [String] latitude and longitude, separated by comma
  # @param options [Hash] :type, :language
  # @return [String] valid url
  def get_search_url(output, location, options={})
    url = "#{GOOGLE_URL}#{output}?location=#{location}&rankby=distance"
    url += "&language=#{LANG_CODE[options[:language]]}" if options[:language]
    url += "&type=#{options[:type]}" if options[:type]
    url + "&key=#{API_KEY}"
  end

  # @param latitude [Float]
  # @param longitude [Float]
  # @return [String] returns coordinates as url-valid, comma-separated string
  def get_location_str(latitude, longitude)
    "#{latitude.to_s},#{longitude.to_s}"
  end

  # @param json [String] json response from google api
  # @return [Array] array of json Hash objects, one for each location
  # @return [String, NilClass] page token for next results if it exists, nil otherwise
  def json_to_array(json)
    response_hash = JSON.parse(File.read json)
    return response_hash['results'], response_hash['next_page_token']
  end

  # @param xml [String] xml response from google api
  # @return [Array] array of XML objects, one for each location
  # @return [String, NilClass] page token for next results if it exists, nil otherwise
  def xml_to_array(xml)
    response_xml = Nokogiri::XML(xml)
    ary = response_xml.xpath('//result')
    page_token_node = response_xml.xpath('//next_page_token').first
    page_token = page_token_node ? page_token_node.text : nil
    return ary, page_token
  end
end